import requests
import os


def ispstack_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "ISPSTACK_API_URL", default="https://ispstack-api.example.com/"
        )

    return settings.ISPSTACK_API_URL


def ispstack_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("ISPSTACK_API_USER", default="user")
        password = os.getenv("ISPSTACK_API_PASSWD", default="secret")
        return username, password

    username = settings.ISPSTACK_API_USER
    password = settings.ISPSTACK_API_PASSWD

    return username, password


def ispstack_head(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.head(
        f"{ispstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def ispstack_get_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.get(
        f"{ispstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def ispstack_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{ispstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def ispstack_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{ispstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def ispstack_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{ispstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def ispstack_delete_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{ispstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def ispstack_get_api_credentials_for_customer(mdat_id: int):
    username, password = ispstack_default_credentials()

    if username != mdat_id:
        response = ispstack_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password


def ispstack_internet_availability_check_by_address(
    mdat_id: int,
    country_iso: str,
    zip_code: str,
    street: str,
    house_nr: str,
    house_nr_add: str = None,
):
    username, password = ispstack_get_api_credentials_for_customer(mdat_id)

    request_data = dict()
    request_data["country_iso"] = country_iso
    request_data["zip_code"] = zip_code
    request_data["street"] = street
    request_data["house_nr"] = house_nr

    if house_nr_add is not None:
        request_data["house_nr_add"] = house_nr_add

    response = ispstack_post_data(
        "v1/accessproductavailabilityaddress",
        username,
        password,
        data=request_data,
    )

    if "available_products" not in response:
        return list()

    return sorted(
        response["available_products"], key=lambda x: x["score"], reverse=True
    )
